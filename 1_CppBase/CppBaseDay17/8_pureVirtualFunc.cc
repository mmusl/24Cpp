#include <iostream>

using std::cout;
using std::endl;

//声明了纯虚函数的类称为抽象类，抽象类是不能创建对象的
//不能创建抽象类的对象，但是也创建抽象类的指针或者引用
class Base
{
public:
    //纯虚函数，留个派生类进行实现的
    virtual void show() const = 0;
    virtual void print() const = 0;
};

//抽象类的派生类中，如果也有纯虚函数没有实现，那么抽象类的
//派生类也是抽象类，抽象类也是不能创建对象的
class Derived
: public Base
{
public:
    void show()  const override//表明是重写
    {
        cout << "void Derived::show() const" << endl;
    }
};

class Derived2
: public Derived
{
public:
    virtual void print() const 
    {
        cout << "void Derived2::print()" << endl;
    }
};

void test()
{
    Derived2 derived2;
    derived2.print();
    derived2.show();
    /* Derived derived;//error */
    /* derived.show(); */

    cout << endl;
    /* Base base;//error */
    /* int *pInt;//ok */
    Base *pbase = &derived2;
    pbase->show();
    pbase->print();

    Derived *pderived = &derived2;
    pderived->show();
    pderived->print();
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

