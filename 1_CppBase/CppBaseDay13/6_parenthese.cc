#include <iostream>

using std::cout;
using std::endl;

//如果一个类中重载了函数调用运算符，那么该类创建的对象
//称为函数对象
class FunctionObject
{
public:
    FunctionObject()
    : _cnt(0)
    {

    }

    int operator()(int x, int y)
    {
        cout << "int operator()(int, int)" << endl;
        ++_cnt;
        return x + y;
    }

    int operator()(int x, int y, int z)
    {
        cout << "int operator()(int, int, int)" << endl;
        ++_cnt;
        return x * y * z;
    }

    void print()
    {
        cout << "void print()" << endl;
    }
private:
    int _cnt;//函数对象的状态
};

int add(int x, int y)
{
    cout << "int add(int, int)" << endl;
    static int cnt = 0;
    ++cnt;
    return x + y;
}
void test()
{
    int a = 3, b = 4, c = 5;
    FunctionObject fo;
    /* fo.operator()(a, b);//原始版本 */
    cout << "fo(a, b) = " << fo(a, b) << endl;
    /* fo.operator()(a, b, c);//原始版本 */
    cout << "fo(a, b, c) = " << fo(a, b, c) << endl;
    fo.print();
    cout << "add(a, b) = " << add(a, b) << endl;

    FunctionObject()(a, b);//ok
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

