#include <iostream>

using std::cout;
using std::endl;

class Complex
{
    friend Complex operator+(const Complex &lhs, const Complex &rhs);
public:
    Complex(double dreal = 0.0, double dimag = 0.0)
    : _dreal(dreal)
    , _dimag(dimag)
    {
        cout << "Complex(double = 0.0, double = 0.0)" << endl;
    }

    //c3 = c1;
    //c3 += c1;
    //复合赋值运算符
    //对象本身会发生改变的情况，都将这种运算符设置为成员函数
    //形式
    //+=/-=/*=/ /= / 
    Complex &operator+=(const Complex &rhs)
    {
        cout << "Complex &operator+=(const Complex &)" << endl;
        _dreal += rhs._dreal;
        _dimag += rhs._dimag;

        return *this;
    }

    double getReal() const
    {
        return _dreal;
    }

    double getImag() const
    {
        return _dimag;
    }

    void print() const
    {
        cout << _dreal << " + " << _dimag << "i" << endl;
    }

    ~Complex()
    {
        cout << "~Complex()" << endl;
    }
private:
    double _dreal;
    double _dimag;
};

Complex operator+(const Complex &lhs, const Complex &rhs)
{
    cout << "friend Complex operator+(const Complex &, const Complex &)" << endl;
    return Complex(lhs._dreal + rhs._dreal, lhs._dimag + rhs._dimag);
}

void test()
{
    Complex c1(1, 3);
    cout << "c1 = ";
    c1.print();

    cout << endl;
    Complex c2(2, 6);
    cout << "c2 = ";
    c2.print();

    cout << endl;
    Complex c3 = c1 + c2;
    cout << "c3 = ";
    c3.print();

    /* int a = 3; */
    /* int b = 4; */
    /* int c = a + b; */
    /* c += a; */
    cout << endl;
    c3 += c1;//ok
    /* c3.operator+=(c1);//ok */
    cout << "c3 = ";
    c3.print();
}


int main(int argc, char *argv[])
{
    test();
    return 0;
}

