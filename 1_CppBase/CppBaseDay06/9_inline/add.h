#ifndef __ADD_H__ //防止在同一个文件中被多次编译
#define __ADD_H__

//对于内联函数而言，不能分成头文件与实现文件，也就是不能将
//声明与实现分开,也就是在链接的时候，链接不到函数的实现
//如果一定要将头文件与实现文件分开，那么可以在头文件中
//去#include实现文件
inline
int add(int, int);

#include "add.cc"

#endif
