#include <math.h>
#include <iostream>

using std::cout;
using std::endl;

class Point
{
    //友元不受访问权限的控制
    friend double distance(const Point &lhs, const Point &rhs);
public:
    Point(int ix = 0, int iy = 0)
    : _ix(ix)
    , _iy(iy)
    {
        cout << "Point(int = 0, int = 0)" << endl;
    }

    void print() const
    {
        cout << "(" << this->_ix
             << ", " << this->_iy
             << ")";
    }

    ~Point()
    {
        cout << "~Point()" << endl;
    }
private:
    int _ix;
    int _iy;
};

//友元的第一种形式：普通函数(全局函数、自由函数)的形式
double distance(const Point &lhs, const Point &rhs)
{
    return hypot(lhs._ix - rhs._ix, lhs._iy - rhs._iy);
    /* int tmp1 = lhs._ix - rhs._ix; */
    /* int tmp2 = lhs._iy - rhs._iy; */
    /* return sqrt(tmp1 * tmp1 + tmp2 * tmp2); */
}

void test()
{
    Point pt1(1, 2);
    Point pt2(4, 6);
    //计算两个点之间的距离
    pt1.print();
    cout << "---->";
    pt2.print();
    cout << "之间的距离 " << distance(pt1, pt2) << endl;
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

