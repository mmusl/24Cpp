#include <iostream>

using std::cout;
using std::endl;

class Point
{
public:
    Point(int ix = 0, int iy = 0)
    : _ix(ix)
    , _iy(iy)
    {
        cout << "Point(int = 0, int = 0)" << endl;
        /* _ix = ix;//赋值 */
        /* _iy = iy;//赋值 */
    }

    void print()
    {
        cout << "(" << this->_ix
             << ", " << this->_iy
             << ")" << endl;
    }

    ~Point()
    {
        cout << "~Point()" << endl;
    }
private:
    const int _ix;
    const int _iy;
};

void test()
{
    Point pt(1, 2);
    cout << "pt = ";
    pt.print();
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

