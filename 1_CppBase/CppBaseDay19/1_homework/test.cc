#include "tinyxml2.h"
#include <iostream>
#include <regex>
#include <string>

using std::cout;
using std::endl;
using std::cerr;
using std::regex;
using std::string;
using namespace tinyxml2;

void test()
{
    XMLDocument doc;
    doc.LoadFile("coolshell.xml");

    if(doc.ErrorID())
    {
        cerr << "LoadFile error" << endl;
        return;
    }

    XMLElement *pNode = doc.FirstChildElement("rss")->FirstChildElement("channel")->FirstChildElement("item");
    if(pNode)
    {
        XMLElement *title = pNode->FirstChildElement("title");
        XMLElement *link = pNode->FirstChildElement("link");
        XMLElement *description = pNode->FirstChildElement("description");
        XMLElement *content = pNode->FirstChildElement("content:encoded");

        if(title)
        {
            const char *ptitle = title->GetText();
            /* cout << "ptitle = " << ptitle << endl; */
        }
        else
        {
            cout << "nullptr == " << endl;
        }

        if(link)
        {
            const char *plink= link->GetText();
            /* cout << "plink= " << plink<< endl; */
        }
        else
        {
            cout << "nullptr == " << endl;
        }

        const char *pdes = nullptr;
        if(description)
        {
            pdes = description->GetText();
            /* cout << "pdes = " << pdes << endl; */
        }
        else
        {
            cout << "nullptr == " << endl;
        }

        if(content)
        {
            const char *pcontent = content->GetText();
            /* cout << "pcontent = " << pcontent << endl; */
        }
        else
        {
            cout << "nullptr == " << endl;
        }

        regex reg("<[^>]+>");
        string msg = regex_replace(string(pdes), reg, "");
        cout << "msg = " << msg << endl;
    }

}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

