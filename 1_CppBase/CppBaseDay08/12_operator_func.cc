#include <iostream>

using std::cout;
using std::endl;

class Point
{
public:
    Point(int ix = 0, int iy = 0)
    : _ix(ix)
    , _iy(iy)
    {
        cout << "Point(int = 0, int = 0)" << endl;
    }

    Point(const Point &rhs)
    : _ix(rhs._ix)
    , _iy(rhs._iy)
    {
        cout << "Point(const Point &)" << endl;
    }

    //默认情况下，编译器会自动生成赋值运算符函数
    Point &operator=(const Point &rhs)
    {
        cout << "Point &operator=(const Point &)" << endl;
        _ix = rhs._ix;//赋值
        _iy = rhs._iy;//赋值

        return *this;
    }

    void print()
    {
        cout << "(" << this->_ix
             << ", " << this->_iy
             << ")" << endl;
    }

    ~Point()
    {
        cout << "~Point()" << endl;
    }
private:
    int _ix;
    int _iy;
};

void test()
{
    Point pt(1, 2);
    cout << "pt = ";
    pt.print();

    cout << endl;
    Point pt2(3, 4);
    cout << "pt2 = ";
    pt2.print();

    cout << endl;
    pt2 = pt;//赋值
    /* pt2.operator=(pt);//赋值运算符函数 */
    cout << "pt2 = ";
    pt2.print();
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

