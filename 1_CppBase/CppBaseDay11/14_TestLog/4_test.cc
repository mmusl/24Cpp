#include <iostream>
#include <log4cpp/BasicLayout.hh>
#include <log4cpp/SimpleLayout.hh>
#include <log4cpp/PatternLayout.hh>
#include <log4cpp/OstreamAppender.hh>
#include <log4cpp/FileAppender.hh>
#include <log4cpp/RollingFileAppender.hh>
#include <log4cpp/Category.hh>
#include<log4cpp/Priority.hh>

using std::cout;
using std::endl;
using namespace log4cpp;

void test()
{
    //日志的格式
    PatternLayout *ppl = new PatternLayout();
    ppl->setConversionPattern("%d [%p] %c: %m%n");

    PatternLayout *ppl2 = new PatternLayout();
    ppl2->setConversionPattern("%d [%p] %c: %m%n");
    
    //日志的目的地
    OstreamAppender *poa =  new OstreamAppender("2", &cout);
    poa->setLayout(ppl);

    FileAppender *pfl = new FileAppender("file", "wd.txt");
    pfl->setLayout(ppl2);

    //日志记录器
    Category &root = Category::getRoot().getInstance("cat");
    root.addAppender(poa);
    root.addAppender(pfl);

    //日志的过滤器
    root.setPriority(Priority::ERROR);

    size_t idx = 10000;
    while(idx > 0)
    {
        root.emerg("This is an emerg message");
        root.fatal("This is an fatal message");
        root.alert("This is an alert essage");
        root.crit("This is an crit message");
        root.error("This is an error message");
        root.notice("This is an notice message");
        root.debug("This is an debug message");
        --idx;
    }

    //回收
    Category::shutdown();
}

void test2()
{
    //日志的格式
    PatternLayout *ppl = new PatternLayout();
    ppl->setConversionPattern("%d [%p] %c: %m%n");

    PatternLayout *ppl2 = new PatternLayout();
    ppl2->setConversionPattern("%d [%p] %c: %m%n");
    
    //日志的目的地
    OstreamAppender *poa =  new OstreamAppender("2", &cout);
    poa->setLayout(ppl);

    RollingFileAppender *prfl = new RollingFileAppender("file", "wd.txt",
                                                5 * 1024,
                                                3);
    prfl->setLayout(ppl2);

    //日志记录器
    Category &root = Category::getRoot().getInstance("cat");
    root.addAppender(poa);
    root.addAppender(prfl);

    //日志的过滤器
    root.setPriority(Priority::ERROR);

    size_t idx = 40;
    while(idx > 0)
    {
        root.emerg("This is an emerg message");
        root.fatal("This is an fatal message");
        root.alert("This is an alert essage");
        root.crit("This is an crit message");
        root.error("This is an error message");
        root.notice("This is an notice message");
        root.debug("This is an debug message");
        --idx;
    }

    //回收
    Category::shutdown();
}
int main(int argc, char *argv[])
{
    test2();
    return 0;
}

