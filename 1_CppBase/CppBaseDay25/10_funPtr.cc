#include <iostream>

using std::cout;
using std::endl;

int func()
{
    cout << "int func()" << endl;
    return 10;
}

int func2()
{
    cout << "int func2()" << endl;
    return 20;
}

int func3(int x, int y)
{
    cout << "int func3(int, int)" << endl;
    return x + y;
}

void test()
{
    /* typedef int (*pFunc)();//ok,C的写法 */
    /* typedef int (*)() pFunc;//error,错误的语法 */
    using pFunc = int (*)();//C++的写法

    func();
    cout << "hello world" << endl;

    //延迟了函数func的调用:C++中的多态就是延迟调用的思想
    //将函数的注册与执行分开了
    //回调函数
    //注册回调函数与执行回调函数
    cout << endl;
    pFunc f = func;//func注册给了f
    //....
    //.....
    cout << "hello kiki" << endl;
    cout <<"f() = " << f() << endl;//执行了f，也就是执行func

    cout << endl;
    f = &func2;
    cout <<"f() = " << f() << endl;

    cout << endl;
    /* f = &func3;//error */
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

