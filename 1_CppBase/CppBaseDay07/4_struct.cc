#include <string.h>
#include <iostream>

using std::cout;
using std::endl;

//C++中结构体的功能做了提升，可以定义变量也可以定义函数
struct Comupter//类名
{
/* public: */
/* private: */
    //成员函数
    //设置电脑的品牌
    void setBrand(const char *brand)
    {
        //n + shift + k
        strcpy(_brand, brand);//没有考虑内存越界
    }

    //设置电脑的价格
    void setPrice(float price)
    {
        _price = price;
    }
    
    //打印电脑的品牌与价格
    void print()
    {
        cout << "brand = " << _brand << endl
             << "price = " << _price << endl;
    }

/* private: */
    //数据成员
    char _brand[20];//品牌
    /* char *_brand;//品牌 */
    float _price;//价格
};//一定不要忘了，表明这一句结束了

int main(int argc, char *argv[])
{
    //通过类名Computer创建了对象com
    Comupter com;
    /* com.setBrand("Thinkpad"); */
    /* com.setPrice(5500); */
    /* com.print(); */
    strcpy(com._brand, "mac");
    com._price = 5500;

    return 0;
}

