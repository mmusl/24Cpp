#include <string.h>
#include <iostream>

using std::cout;
using std::endl;

class Base
{
public:
    Base()
    : _pbase(nullptr)
    {
        cout << "Base()" << endl;
    }

    Base(const char *pbase)
    : _pbase(new char[strlen(pbase) + 1]())
    {
        cout <<"Base(const char *)" << endl;
        strcpy(_pbase, pbase);
    }

    Base(const Base &rhs)
    : _pbase(new char[strlen(rhs._pbase) + 1]())
    {
        cout << "Base(const Base &)" << endl;
        strcpy(_pbase, rhs._pbase);
    }

    Base &operator=(const Base &rhs)
    {
        cout << "Base &operator=(const Base &)" << endl;
        if(this != &rhs)
        {
            delete [] _pbase;
            _pbase = nullptr;

            _pbase = new char[strlen(rhs._pbase) + 1]();
            strcpy(_pbase, rhs._pbase);
        }

        return *this;
    }

    ~Base()
    {
        cout << "~Base()" << endl;
        if(_pbase)
        {
            delete [] _pbase;
            _pbase = nullptr;
        }
    }

    friend std::ostream &operator<<(std::ostream &os, const Base &rhs);
private:
    char *_pbase;
};

std::ostream &operator<<(std::ostream &os, const Base &rhs)
{
    if(rhs._pbase)
    {
        os << rhs._pbase;
    }

    return os;
}

class Derived
: public Base
{
public:
    Derived(const char *pbase)
    : Base(pbase)
    {
        cout << "Derived(const char *)" << endl;
    }

    ~Derived()
    {
        cout << "~Derived()" << endl;
    }
    friend std::ostream &operator<<(std::ostream &os, const Derived &rhs);
};

std::ostream &operator<<(std::ostream &os, const Derived &rhs)
{
    const Base &ref = rhs;
    os << ref;

    return os;
}

void test()
{
    Derived derived1("hello");
    cout << "derived1 = " << derived1 << endl;

    cout << endl;
    Derived derived2 = derived1;//拷贝
    cout << "derived1 = " << derived1 << endl;
    cout << "derived2 = " << derived2 << endl;

    cout << endl;
    Derived derived3("word");
    cout << "derived3 = " << derived3 << endl;

    cout << endl;
    derived3 = derived1;//赋值
    cout << "derived1 = " << derived1 << endl;
    cout << "derived3 = " << derived3 << endl;

}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

