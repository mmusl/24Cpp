#include <iostream>
#include <memory>

using std::cout;
using std::endl;
using std::shared_ptr;
using std::weak_ptr;

class Child;//前向声明

class Parent
{
public:
    Parent()
    {
        cout << "Parent()" << endl;
    }

    ~Parent()
    {
        cout << "~Parent()" << endl;
    }

    weak_ptr<Child> _pChild;//弱引用的智能指针
};

class Child
{
public:
    Child()
    {
        cout << "Child()" << endl;
    }

    ~Child()
    {
        cout << "~Child()" << endl;
    }
    shared_ptr<Parent> _pParent;
};

void test()
{
    shared_ptr<Parent> spParent(new Parent());//栈对象
    shared_ptr<Child> spChild(new Child());//栈对象
    cout << "spParent.use_count() = " << spParent.use_count() << endl;
    cout << "spChild.use_count() = " << spChild.use_count() << endl;

    cout << endl;
    //代码有什么缺陷？
    //Q：内存泄漏？
    spParent->_pChild = spChild;//赋值
    spChild->_pParent = spParent;//赋值
    cout << "spParent.use_count() = " << spParent.use_count() << endl;
    cout << "spChild.use_count() = " << spChild.use_count() << endl;
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

