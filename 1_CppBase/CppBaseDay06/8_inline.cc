#include <iostream>

using std::cout;
using std::endl;
//宏定义发生的时机在预处理阶段，一旦有bug，就会到运行
//的时候才会发现
#define MULTIPLY(x, y) ((x) * (y))

//普通函数的调用是有开销的，参数的入栈出栈，保存函数调用的
//现场

//在函数调用处，直接使用函数体去替代函数的调用
//提出内联函数的作用：就是为了减少开销，提高程序的效率
//
//内联函数规定：内联函数是小函数，函数体是比较简单的
//不能有函数的调用，不能有for/while
//
//现代的编译器比较智能，可以自动识别函数是不是内联函数
inline 
int add(int x, int y)
{
    return x + y;
}

void test()
{
    int a = 3, b = 4;
    cout << "add(a, b) = " << add(a, b) << endl;
    cout << "MULTIPLY(a, b) = " << MULTIPLY(a, b) << endl;
    //...
    //...
}

//C/C++从main函数入手
int main(int argc, char *argv[])
{
    test();
    return 0;
}

