#include <iostream>
#include <vector>

using std::cout;
using std::endl;
using std::vector;

template <typename Container>
void display(const Container &con)
{
    for(auto &elem : con)
    {
        cout << elem << "  ";
    }
    cout << endl;
}

void printCapacity(const vector<int> &con)
{
    cout << "size() = " << con.size() << endl;
    cout << "capacity() = " << con.capacity() << endl;
}

class Point
{
public:
    Point(int ix = 0, int iy = 0)
    : _ix(ix)
    , _iy(iy)
    {

    }
private:
    int _ix;
    int _iy;
};

void test00()
{
    Point pt(1, 2);
    vector<Point> vec;
    vec.push_back(pt);
    vec.push_back(Point(3, 4));
    vec.emplace_back(1, 2);
}

void test()
{
    cout << "sizeof(vecotr<int>) = " << sizeof(vector<int>) << endl;
    cout << "sizeof(vecotr<long>) = " << sizeof(vector<long>) << endl;

    cout << endl;
    vector<int> number = {1, 2, 4, 6, 8, 9, 3, 6, 7};
    display(number);
    printCapacity(number);

    /* number.reserve(10);//预留空间 */
    /* printCapacity(number); */
    cout << endl << "在vector尾部进行插入与删除" << endl;
    number.push_back(11);
    number.push_back(22);
    display(number);
    printCapacity(number);
    number.pop_back();
    display(number);
    printCapacity(number);

    //对于vector而言，不能在头部进行插入与删除?为什么？
    //vector是一端开口的,只能在尾部进行操作
    //在头部进行插入与删除的时候，会将后面的元素全部进行移动
    //这样会导致开销比较大，时间复杂度会为O(N)
    
    //对于vector而言，如何获取第一个元素的首地址?
    &number;//error,得不到第一个元素的地址
    &*number.begin();//ok
    &number[0];//ok
    int *pdata = number.data();//ok
    
    cout << endl << "在vector的任意位置进行插入" << endl;
    auto it = number.begin();
    ++it;
    ++it;
    cout << "*it = " << *it << endl;
    number.insert(it, 33);
    display(number);
    cout << "*it = " << *it << endl;
    printCapacity(number);

    //迭代器失效，就是底层已经发生了扩容
    //不管迭代器有没有失效，不管底层有没有发生扩容，每次在使用
    //的时候，都将迭代器重新置位即可
    cout << endl;
    it = number.begin();
    ++it;
    ++it;
    cout << "*it = " << *it << endl;
    number.insert(it, 6, 666);
    display(number);
    cout << "*it = " << *it << endl;
    printCapacity(number);

    cout << endl;
    vector<int> vec = {200, 500, 600, 300};
    it = number.begin();
    ++it;
    ++it;
    cout << "*it = " << *it << endl;
    number.insert(it, vec.begin(), vec.end());
    display(number);
    cout << "*it = " << *it << endl;
    printCapacity(number);

    cout << endl;
    it = number.begin();
    ++it;
    ++it;
    cout << "*it = " << *it << endl;
    number.insert(it, {123, 456, 789});
    display(number);
    cout << "*it = " << *it << endl;
    printCapacity(number);

    cout << endl << "清空vector中的元素" << endl;
    number.clear();//清空所有的元素
    number.shrink_to_fit();//回收多余的空间
    printCapacity(number);

}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

