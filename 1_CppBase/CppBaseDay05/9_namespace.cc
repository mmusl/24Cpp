#include <stdio.h>
#include <iostream>

using std::cout;
using std::endl;

namespace wd
{
int number = 20;

void show(int value)
{
    cout << "形参value = " << value << endl;
    cout << "命名空间中number = " << number << endl;
    cout << "命名空间中number = " << wd::number << endl;
}

//命名空间是可以进行嵌套的
namespace wh
{

void print()
{
    cout << "void wd::wh::print()" << endl;
}

}//end of namespace wh

}//end of namespace std

int main(int argc, char *argv[])
{
    int a = 300;
    wd::show(a);

    wd::wh::print();

    return 0;
}

