#include <iostream>

using std::cout;
using std::endl;

class A
{
public:
    A()
    {
        cout <<"A()" << endl;
    }

    void print() const
    {
        cout << "void A::print()" << endl;
    }

    ~A()
    {
        cout <<"~A()" << endl;
    }
};

class B
{
public:
    B()
    {
        cout <<"B()" << endl;
    }

    void print() const
    {
        cout << "void B::print()" << endl;
    }

    ~B()
    {
        cout <<"~B()" << endl;
    }
};

class C
{
public:
    C()
    {
        cout << "C()" << endl;
    }

    void print() const
    {
        cout << "void C::print()" << endl;
    }

    ~C()
    {
        cout <<"~C()" << endl;
    }
};

//对于多继承而言，最好对每个基类都写继承方式，继承方式的关键字
//是不能共用的，如果那个基类不写继承方式，那就是默认的继承方式
//private
//
//对于多继承而言，基类构造函数的执行顺序与其在派生类构造函数的
//初始化列表中没有关系，只与派生类继承基类的基类的先后顺序有关
class D
: public A
, public B
, public C
{
public:
    D()
    {
        cout <<"D()" << endl;
    }

    ~D()
    {
        cout <<"~D()" << endl;
    }
};

void test()
{
    //多继承下，成员函数的访问冲突
    //解决方式:使用类名与作用域限定符的形式
    D d;
    d.A::print();
    d.B::print();
    d.C::print();
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

