#include <iostream>
#include <queue>
#include <vector>

using std::cout;
using std::endl;
using std::priority_queue;
using std::vector;

void test()
{
    //优先级队列的底层使用的是堆排序,默认使用的是大根堆
    //原理：当有新的元素插入进来的时候，会与堆顶进行比较，如果
    //堆顶比新插入的元素要小，就满足std::less,就会将新的元素与
    //上一次的堆顶进行置换，也就是新的元素会成为新的堆顶;如果
    //新插入的元素比堆顶要小，也就是堆顶比新插入的元素要大，
    //也就是不满足std::less,那么新的堆顶还是上一次堆顶
    vector<int> vec = {1, 3, 6, 9, 8, 3, 6, 5};
    /* priority_queue<int> pque(vec.begin(), vec.end()); */
    priority_queue<int> pque;
    /* priority_queue<int, vector<int>, std::greater<int>> pque; */

    for(size_t idx = 0; idx !=  vec.size(); ++idx)
    {
        pque.push(vec[idx]);
        cout << "优先级最高的元素 " << pque.top() << endl;
    }


    while(!pque.empty())
    {
        cout << pque.top() << "  ";
        pque.pop();
    }
    cout << endl;
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

