#include "Mylogger.h"
#include <iostream>

using std::cout;
using std::endl;

void test()
{
    int number = 100;
    const char *pstr = "hello,world";
    logError("This is wangdao message, number = %d, pstr = %s\n",
             number, pstr);
    logWarn("This is warn message");
    logInfo("This is info message");
    logDebug("This is debug message");
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

