#include <iostream>

using std::cout;
using std::endl;

void test()
{
    //常量指针被转化成非常量指针，并且仍然指向原来的对象；
    //常量引用被转换成非常量引用，并且仍然指向原来的对象；
    //常量对象被转换成非常量对象
    const int number = 10;//number是一个常量
    /* int *pInt = &number;//error */
    int *pInt = const_cast<int *>(&number);
    cout << "number = " << number << endl;
    cout << "*pInt = " << *pInt << endl;
    
    *pInt = 30;//未定义的行为
    cout << "number = " << number << endl;
    cout << "*pInt = " << *pInt << endl;
    printf("&number = %p\n", &number);
    printf("&pInt = %p\n", &pInt);
    printf("pInt = %p\n", pInt);
}

void test2()
{
    int number = 10;
    int *pInt = &number;
    *pInt = 30;
    cout << "number = " << number << endl;
    cout << "*pInt = " << *pInt << endl;
    printf("&number = %p\n", &number);
    printf("&pInt = %p\n", &pInt);
    printf("pInt = %p\n", pInt);

}

int main(int argc, char *argv[])
{
    test2();
    return 0;
}

