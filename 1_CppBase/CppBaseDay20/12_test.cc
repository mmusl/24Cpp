#include <iostream>

using std::cout;
using std::endl;

//现在想让大家计算1 +2 + ... + 100
void test()
{
    //以前的写法
    size_t tmp = 0;
    for(size_t idx = 1; idx < 101; ++idx)
    {
        tmp += idx;
    }
}

int sum()
{
    return 0;
}

template <typename T, typename ...A>
int sum(T x, A ...a)
{
    return x + sum(a...);
}

void test2()
{
    cout << "sum(1, 2, ....10) = "
         << sum(1, 2, 3, 4, 5, 6, 7, 8, 9, 10) << endl;
         /* << sum(1 ...100) << endl; */
}

int main(int argc, char *argv[])
{
    test2();
    return 0;
}

