#include <math.h>
#include <iostream>

using std::cout;
using std::endl;

class Point;//类的前向声明

class Line
{
public:
    //对于重载函数而言，虽然函数名字是一样，将其中一个作为友元，另外
    //如果也想访问另外类中的私有成员，也要单独设置成友元
    double distance(const Point &lhs, const Point &rhs);
    double distance(const Point &rhs);
};

class Point
{
    //友元的第二种形式：友元之成员函数的形式
    friend double Line::distance(const Point &lhs, const Point &rhs);
    friend double Line::distance(const Point &rhs);
public:
    Point(int ix = 0, int iy = 0)
    : _ix(ix)
    , _iy(iy)
    {
        cout << "Point(int = 0, int = 0)" << endl;
    }

    void print() const
    {
        cout << "(" << this->_ix
             << ", " << this->_iy
             << ")";
    }

    ~Point()
    {
        cout << "~Point()" << endl;
    }
private:
    int _ix;
    int _iy;
};

double Line::distance(const Point &lhs, const Point &rhs)
{
    return hypot(lhs._ix - rhs._ix, lhs._iy - rhs._iy);
}

double Line::distance(const Point &rhs)
{
    return hypot(rhs._ix, rhs._iy);
}
void test()
{
    Line line;

    Point pt1(1, 2);
    Point pt2(4, 6);
    //计算两个点之间的距离
    pt1.print();
    cout << "---->";
    pt2.print();
    /* cout << "之间的距离 " << Line().distance(pt1, pt2) << endl; */
    cout << "之间的距离 " << line.distance(pt1, pt2) << endl;
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

