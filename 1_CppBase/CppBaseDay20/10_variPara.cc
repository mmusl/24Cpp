#include <iostream>

using std::cout;
using std::endl;

/* template <typename T1, typename T2, typename T3> */
/* void print(T1 t1, T2 t2, T3 t3) */
template <typename ...Args>//模板参数包
void print(Args ...args)//函数参数包
{
    cout << "sizeof(Args) = " << sizeof...(Args) << endl;
    cout << "sizeof(args) = " << sizeof...(args) << endl;
    /* cout << args... << endl; */
    /* cout << ...args << endl; */
    /* cout << args << endl; */
}

#if 0
//T1将其推导成string
//T2将其推导为double
//T3将其推导为bool
//函数参数中的x1 = "hello", x2 = 3.3, x3 = true
template <typename T1, typename T2, typename T3>
void print(T1 x1, T2 x2, T3 x3)
{

}
#endif
void test()
{
    //函数没有参数
    print();
    //int 1   string "hello"
    print(1, "hello");
    print("hello", 3.3, true);
    print("hello", 3.3, true, 4.4, 5.5, "world");
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

