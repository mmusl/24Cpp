#include <string.h>
#include <iostream>

using std::cout;
using std::endl;

class Comupter 
{
public:    
    inline
    void setBrand(const char *brand);
    inline 
    void setPrice(float price);
    inline
    void print();

private:
    char _brand[20];
    float _price;
};

//类中的成员函数是可以在类外面进行实现的
void Comupter::setBrand(const char *brand)
{
    strcpy(_brand, brand);//没有考虑内存越界
}

void Comupter::setPrice(float price)
{
    _price = price;
}

void Comupter::print()
{
    cout << "brand = " << _brand << endl
         << "price = " << _price << endl;
}
int main(int argc, char *argv[])
{
    //通过类名Computer创建了对象com
    Comupter com;
    com.setBrand("Thinkpad");
    com.setPrice(5500);
    com.print();
    /* com._price = 5500;//error */

    return 0;
}

