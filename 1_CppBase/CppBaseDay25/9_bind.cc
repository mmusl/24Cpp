#include <iostream>
#include <functional>

using std::cout;
using std::endl;
using std::bind;

int add(int x, int y)
{
    cout << "int add(int, int)" << endl;
    return x + y;
}

int func(int x, int y, int z)
{
    cout << "int func(int, int, int)" << endl;
    return x * y * z;
}

class Example
{
public:
    /* static int add(int x, int y) */
    //隐含的this指针
    int add(int x, int y)
    {
        cout << "int Example::add(int, int)" << endl;
        return x + y;
    }
};

void test()
{
    //C中的函数，函数名就是函数的入口地址
    //函数的形态（类型）:函数的返回类型 + 参数列表
    //函数的参数列表：参数的个数、参数的顺序、参数的类型
    //int(int, int)--->int()
    /* auto f = bind(add, 1, 3); */
    auto f = bind(&add, 1, 3);
    cout << "f() = " << f() << endl;

    cout << endl;
    auto f2 = bind(func, 10, 20, 30);
    cout << "f2() = " << f2() << endl;

    cout << endl;
    Example ex;
    auto f3 = bind(&Example::add, &ex, 30, 70);
    cout <<"f3() = " << f3() << endl;
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

