#include <string.h>
#include <iostream>

using std::cout;
using std::endl;

class String
{
public:
    String()
    : _pstr(nullptr)
    {
        cout << "String()" << endl;
    }

    String(const char *pstr)
    : _pstr(new char[strlen(pstr) + 1]())
    {
        cout << "String(const char *)" << endl;
        strcpy(_pstr, pstr);
    }

    //将拷贝构造函数与赋值运算符函数称为具备拷贝语义的函数
    //将移动构造函数与移动赋值函数称为具有移动语义的函数
    //
    //具有移动语义的函数优先于具备拷贝语义的函数执行
    //
    //拷贝构造函数
    String(const String &rhs)
    : _pstr(new char[strlen(rhs._pstr) + 1]())
    {
        cout << "String(const String &)" << endl;
        strcpy(_pstr, rhs._pstr);
    }

    //赋值运算符函数
    String &operator=(const String &rhs)
    {
        cout << "String &operator=(const Stirng &)" << endl;
        if(this != &rhs)
        {
            delete [] _pstr;
            _pstr = nullptr;
             
            _pstr = new char[strlen(rhs._pstr) + 1]();
            strcpy(_pstr, rhs._pstr);
        }

        return *this;
    }

    //String s1 = String("hello")
    //移动构造函数
    String(String &&rhs)
    : _pstr(rhs._pstr)
    {
        cout << "String(String &&)" <<endl;
        rhs._pstr = nullptr;
    }

    //移动赋值(运算符)函数
    //s2 = String("wangdao")
    String &operator=(String &&rhs)
    {
        cout << "String &operator=(String &&)" << endl;
        if(this != &rhs)//1、自移动
        {
            cout << "0000000" << endl;
            delete [] _pstr;//2、释放操作数
            _pstr = nullptr;

            _pstr = rhs._pstr;//3、浅拷贝
            rhs._pstr = nullptr;
        }

        return *this;
    }

    ~String()
    {
        cout << "~String()" << endl;
        if(_pstr)
        {
            delete [] _pstr;
            _pstr = nullptr;
        }
    }

    friend std::ostream &operator<<(std::ostream &os, const String &rhs);
private:
    char *_pstr;
};

std::ostream &operator<<(std::ostream &os, const String &rhs)
{
    if(rhs._pstr)
    {
        os << rhs._pstr;
    }

    return os;
}

void test()
{
    String s1("hello");
    cout << "s1 = " << s1 << endl;

    cout << endl;
    String s2 = s1;//拷贝构造函数
    cout << "s1 = " << s1 << endl;
    cout << "s2 = " << s2 << endl;

    cout << endl;
    String s3("world");
    cout << "s3 = " << s3 << endl;

    cout << endl;
    s3 = s1;//赋值运算符
    cout << "s1 = " << s1 << endl;
    cout << "s3 = " << s3 << endl;
}

void test2()
{
    //C风格字符串-----> C++风格字符串
    String s1 = "hello";//隐式转换 String("hello")
    //String("hello"),临时对象/匿名对象
    /* &String("hello"); */
    cout << "s1 = " << s1 << endl;

    cout << endl;
    String s2("world");
    cout << "s2 = " << s2 << endl;

    cout << endl;
    /* s2 = s1; */
    s2 = String("wangdao");
    cout << "s2 = " << s2 << endl;

    cout << endl;
    String("beijing") =  String("beijing");

    cout << endl << "std::move()===" << endl;
    //std::move可以将左值转换为右值，但是本身是没有什么移动
    //的含义,本质上只是做了一个强转static_cast<T &&>(value)
    s2 = std::move(s2);
    /* &s2;//ok */
    cout << "s2 = " << s2 << endl;
}

int main(int argc, char *argv[])
{
    test2();
    return 0;
}

