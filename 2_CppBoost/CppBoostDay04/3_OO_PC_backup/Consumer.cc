#include "Consumer.h"
#include "TaskQueue.h"
#include <unistd.h>
#include <iostream>

using std::cout;
using std::endl;

Consumer::Consumer(TaskQueue &taskQue)
: _taskQue(taskQue)
{

}

Consumer::~Consumer()
{

}

void Consumer::run() 
{
    size_t cnt = 20;
    while(cnt--)
    {
        //消费数据
        int number = _taskQue.pop();
        cout << ">>Consumer number = " << number << endl;
        sleep(1);
    }
}
