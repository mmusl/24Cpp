#include "Producer.h"
#include "TaskQueue.h"
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <iostream>

using std::cout;
using std::endl;

Producer::Producer(TaskQueue &taskQue)
: _taskQue(taskQue)
{

}

Producer::~Producer()
{

}

void Producer::run() 
{
    //种随机种子
    ::srand(::clock());//::匿名命名空间

    size_t cnt = 20;
    while(cnt--)
    {
        //生产者生产数据(随机数)
        int number = rand() % 100;//产生[0, 99]
        _taskQue.push(number);
        cout << ">>Producer number = " << number << endl;
        sleep(1);
    }
}
