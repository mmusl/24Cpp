#include "Computer.h"
#include <iostream>

using std::cout;
using std::endl;

void test()
{
    Comupter com("Thinkpad", 5500);//com是栈对象
    cout << "com = ";
    com.print();

    cout << endl;
    Comupter com2 = com;//拷贝构造函数
    cout << "com2 = ";
    com2.print();

    cout << endl;
    Comupter com3("mac", 20000);//拷贝构造函数
    cout << "com3 = ";
    com3.print();

    cout << endl << "com3 = com" << endl;
    com3 = com;//赋值运算符函数
    cout << "com3 = ";
    com3.print();

    cout << endl << "com3 = com3" << endl;
    com3 = com3;//赋值运算符函数
    cout << "com3 = ";
    com3.print();
}

void test2()
{
    cout << "sizeof(Comupter) = " << sizeof(Comupter) << endl;

    cout << endl;
    cout << "在购买电脑之前，电脑总价" << endl;
    Comupter::printTotalPrice();

    cout << endl;
    Comupter com("Thinkpad", 5500);//com是栈对象
    cout << "com = ";
    com.print();
    cout << "买第一台电脑的时候的总价";
    com.printTotalPrice();
    Comupter::printTotalPrice();

    cout << endl;
    Comupter com2("mac", 20000);//拷贝构造函数
    cout << "com2 = ";
    com2.print();
    cout << "买第二台电脑的时候的总价" ;
    com.printTotalPrice();
    com2.printTotalPrice();
    Comupter::printTotalPrice();
}

void test3()
{
    /* int number = 10; */
    /* const int number2 = 100; */
    //1、非const对象默认调用的是非const版本的成员函数，而const
    //对象默认调用的是const版本的成员函数
    //2、如果没有非const版本的成员函数，那么const对象与非const
    //对象都只会调用const版本的成员函数
    //3、const对象不能调用非const版本的成员函数,非const的对象
    //是可以调用非const版本的成员函数
    Comupter com("Thinkpad", 5500);//com是栈对象
    cout << "com = ";
    com.print();

    cout << endl;
    const Comupter com2("mac", 20000);//拷贝构造函数
    cout << "com2 = ";
    com2.print();
}
int main(int argc, char *argv[])
{
    test3();
    return 0;
}

