#include "Computer.h"
#include <string.h>
#include <iostream>

using std::cout;
using std::endl;

//静态数据成员不能在初始化列表中进行，必须要放在类的外面
//进行初始化，特别的：对于头文件与实现文件的形式，还要将
//静态数据成员放在实现文件中进行初始化，否则会出现多次
//定义的问题
float Comupter::_totalPrice = 0;

Comupter::Comupter(const char *brand, float price)
: _brand(new char[strlen(brand) + 1]())//申请了空间
, _price(price)
/* , _totalPrice(0)//error */
{
    cout << "Comupter(const char *, float)" << endl;
    strcpy(_brand, brand);
    _totalPrice += _price;
}
#if 0
//编译器生成的拷贝构造函数
Comupter::Comupter(const Comupter &rhs)
: _brand(rhs._brand)//浅拷贝
, _price(rhs._price)
{
    cout << "Comupter(const Comupter &)" << endl;
}
#endif

Comupter::Comupter(const Comupter &rhs)
: _brand(new char[strlen(rhs._brand) + 1]())//深拷贝
, _price(rhs._price)
{
    cout << "Comupter(const Comupter &)" << endl;
    strcpy(_brand, rhs._brand);
}

#if 0
Comupter &Comupter::operator=(const Comupter &rhs)
{
    cout << "Comupter &operator=(const Comupter &)" << endl;
    _brand = rhs._brand;//浅拷贝
    _price = rhs._price;

    return *this;
}
#endif
//四步曲
Comupter &Comupter::operator=(const Comupter &rhs)
{
    cout << "Comupter &operator=(const Comupter &)" << endl;
    //左操作数    右操作数
    //如果是自己赋值给自己
    if(this != &rhs)//1、自复制
    {
        delete [] _brand;//2、释放左操作数
        _brand = nullptr;

        _brand = new char[strlen(rhs._brand) + 1]();//3、深拷贝
        strcpy(_brand, rhs._brand);

        _price = rhs._price;
    }

    return *this;//4、返回*this
}

void Comupter::setBrand(const char *brand)
{
    strcpy(_brand, brand);
}

void Comupter::setPrice(float price)
{
    _price = price;
}

void Comupter::print(/*  Comupter * const this  */)
{
    cout << "void Comupter::print()" << endl;
    if(_brand)
    {
        cout << "brand = " << _brand << endl;
    }
    cout << "price = " << this->_price << endl;
    /* _totalPrice = 100;//ok */
    /* printTotalPrice();//ok */
}

void Comupter::print(/* const Comupter  * const this */) const
{
    cout << "void Comupter::print() const" << endl;
    /* this = nullptr;//error */
    /* this->_price = 100;//error */
    if(_brand)
    {
        cout << "brand = " << _brand << endl;
    }
    cout << "price = " << this->_price << endl;
}

Comupter::~Comupter()
{
    cout << "~Comupter()" << endl;
    if(_brand)
    {
        delete [] _brand;
        _brand = nullptr;
    }
}

//1、静态成员函数的第一个参数的位置没有隐含的this指针
//2、静态成员函数不能访问非静态数据成员和非静态成员函数
//3、非静态成员函数可以访问静态数据成员和静态成员函数
//4、静态成员函数如果想访问非静态的成员可以使用函数传参
//5、静态成员函数除了使用对象进行调用之外，还可以使用
//类名与作用域限定符的形式进行调用
void Comupter::printTotalPrice()
{
    /* this;//error */
    cout << "_totalPrice = " << _totalPrice << endl;
    /* cout << "price = " << _price << endl;//error */
    /* print();//error */
}

