#include <iostream>
#include <fstream>
#include <string>

using std::cout;
using std::endl;
using std::cerr;
using std::ifstream;
using std::ofstream;
using std::string;

void test()
{
    ifstream ifs("test.txt", std::ios::in | std::ios::ate);
    if(!ifs)//if(!ifs.good())
    {
        cerr << "ifstream is not good" << endl;
        return;
    }

    cout << "pos = " << ifs.tellg() << endl;
    ifs.seekg(0);

    string line;
    while(getline(ifs, line))
    {
        cout << line << endl;
    }

    ifs.close();
}

void test2()
{
    ofstream ofs("test.txt", std::ios::out | std::ios::app);
    if(!ofs)//if(!ofs.good())
    {
        cerr << "ofstream is not good" << endl;
        return;
    }

    cout << "pos = " << ofs.tellp() << endl;
    ofs << "hello,world" << endl;
    ofs << "hello,world" << endl;
    ofs << "hello,world" << endl;
    cout << "pos = " << ofs.tellp() << endl;

    ofs.close();
}
int main(int argc, char *argv[])
{
    test2();
    return 0;
}

