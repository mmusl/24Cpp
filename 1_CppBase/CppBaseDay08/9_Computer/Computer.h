#ifndef __COMPUTER_H__
#define __COMPUTER_H__

class Comupter 
{
public:    
    Comupter(const char *brand, float price);
    Comupter(const Comupter &rhs);
    Comupter &operator=(const Comupter &rhs);
    void setBrand(const char *brand);
    void setPrice(float price);
    void print();
    ~Comupter();

private:
    char *_brand;
    float _price;
};

#endif

