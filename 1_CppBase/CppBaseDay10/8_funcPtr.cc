#include <iostream>

using std::cout;
using std::endl;

int func1(int x)
{
    cout << "int func1(int)" << endl;
    return x;
}

int func2(int y)
{
    cout << "int func2(int)" << endl;
    return 2 * y;
}

void test()
{
    int (*pf)(int);//函数指针

    pf = &func1;
    cout << "pf(100) = " << pf(100) << endl;

    cout << endl;
    pf = func2;
    cout << "pf(100) = " << pf(100) << endl;
}

void func4(int (*pt)(int))
{
    cout << "func4 = " << pt(2000) << endl;
}

int main(int argc, char *argv[])
{
    func4(&func1);
    /* test(); */
    /* printf("main = %p\n", main); */
    /* printf("&main = %p\n", &main); */
    return 0;
}

