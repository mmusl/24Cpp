#include <iostream>

using std::cout;
using std::endl;

class Example
{
public:
    Example(int ix = 0, int iy = 0)
    : _ix(ix)
    , _iy(iy)
    {

    }

    //成员函数也是可以设置为模板的
    template <typename T = int>
    T func()
    {
        return (T)_ix;
    }
private:
    int _ix;
    int _iy;
};

void test()
{
    Example ex(10, 20);
    cout << "ex.func() = " << ex.func<int>() << endl;
    cout << "ex.func() = " << ex.func<double>() << endl;
    cout << "ex.func() = " << ex.func() << endl;
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

