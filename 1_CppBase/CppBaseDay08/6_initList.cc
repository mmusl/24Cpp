#include <iostream>

using std::cout;
using std::endl;

class Example
{
public:
    Example(int value)
    : _iy(value)//数据成员的初始化的顺序与其在初始化列表的顺序
    , _ix(_iy)//没有关系，只与其在声明的先后顺序有关
    {
        cout << "Example(int)" << endl;
    }

    void print()
    {
        cout << "(" << _ix
             << ", " << _iy
             << ")" << endl;
    }
private:
    int _iy;
    int _ix;
};

void test()
{
    Example ex(10);
    ex.print();
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

