//C++是在C的基础上发展出来
////C++的代码是使用模板写的：模板的特点是，必须要知道所有的
//实现之后才能进行正常的编译
#include <iostream>

using namespace std;//命名空间

//1、函数的声明与函数的(定义)实现的区别？
//函数的声明可以有多次,但是函数的定义只能有一次
void test();
void test();
void test();
void test();
void test();

void test()
{

}

//注释代码的方法
//常用的三种方法：//、/*  */  、#if 0  #endif
#if 0
void test()
{

}
#endif

int main(int argc, char *argv[])
{
    //cout，是标准输出,输出流的对象
    //<<,输出流运算符
    //endl = end of line,换行
    /* cout << "Hello World\n" ; */
    /* cout << "Hello World" ; */
    /* cout << endl; */
    //快速注释在vimplus下面，可以使用gcc
    cout << "Hello World" << endl;
    //将<<运算符进行了重载
    /* operator<<(cout, "Hello World"); */
    /* cout.operator<<(endl); */
    //C++的语法知识：运算符重载
    /* operator<<(cout, "Hello World").operator<<(endl); */

    int number = 0;
    //cin，标准输入，就是输入流对象
    //>>,输入流运算符
    cin >> number;
    cout << "number = " << number << endl;
    return 0;
}







