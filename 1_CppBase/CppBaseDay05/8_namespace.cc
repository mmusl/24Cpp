#include <stdio.h>
#include <iostream>

using std::cout;
using std::endl;

int number = 1;//全局变量number

namespace wd
{

int number = 20;

void show(int number)
{
    cout << "形参number = " << number << endl;
    cout << "命名空间中number = " << wd::number << endl;
                                  //匿名命名空间
    cout << "全局变量number = " << ::number << endl;
    printf("hello,world\n");
    ::printf("hello,world\n");
}

}//end of namespace std

int main(int argc, char *argv[])
{
    int value = 300;
    wd::show(value);

    return 0;
}

