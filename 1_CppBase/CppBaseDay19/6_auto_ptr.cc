#include <iostream>
#include <memory>

using std::cout;
using std::endl;
using std::auto_ptr;

void test()
{
    //裸指针
    int *pInt = new int(10);
    auto_ptr<int> ap(pInt);//智能指针
    cout << "*pInt = " << *pInt << endl;
    cout << "*ap = " << *ap << endl;
    cout << "pInt = " << pInt << endl;
    cout << "ap.get() = " << ap.get() << endl;

    cout << endl;
    //auto_ptr在执行拷贝构造函数的时候，将ap托管的空间的所有权
    //已经转移给ap2了，然后ap的数据成员已经指向空指针
    //
    //auto_ptr的缺陷是：在执行拷贝构造函数的时候，已经发生了
    //所有权的转移,在复制的时候存在缺陷,就不使用它
    auto_ptr<int> ap2 = ap;//拷贝构造函数
    cout << "*ap2 = " << *ap2 << endl;
    cout << "*ap = " << *ap << endl;
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

