#include <iostream>

using std::cout;
using std::endl;

class Base
{
public:
    Base(long member = 0)
    : _member(member)
    {
    }

    /* virtual */
    void print() 
    {
        cout << "Base::_member = " << _member << endl;
    }

    ~Base()
    {

    }
/* private: */
protected:
    long _member;
};

class Derived
: public Base
{
public:
    Derived(long member = 0, long member2 = 0)
    : Base(member)
    , _member(member2)
    {

    }

    /* void print(); */
    /* virtual */
    void print(int x)
    {
        cout << "x = " << x << endl;
        cout << "void Derived::print(int)" << endl;
        cout << "_member = " << _member << endl;//
        cout << "Base::_member = " << Base::_member << endl;//
    }

    ~Derived()
    {

    }
private:
    long _member;
};
void test()
{
    Derived d(100, 200);
    d.Base::print();//是隐藏了，但不是直接删除了
    /* d.print();//error */
    d.print(22);
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

